console.log("Tinydrum");
var seq = new Sequencer();
var debug = false;
var keyShiftPressed = false;
sequence = [
  { instrument: 0,time: 0 },
  { instrument: 0,time: 3 },
  { instrument: 0,time: 6 },
  { instrument: 0,time: 10 },
  { instrument: 0,time: 12 },
  { instrument: 2,time: 4 },
  { instrument: 3,time: 1 },
  { instrument: 1,time: 8 },
  { instrument: 1,time: 14 },
  { instrument: 1,time: 15 }
];
seq.SetSequence(sequence);

window.addEventListener('load', function () {
  canvas = new Canvas();
  canvas.ConnectSequencer(seq);
})

document.addEventListener('keydown', (event) => {
  if(event.which == 32) {
    StartPause();
    return;
  }
  switch(event.key) {
    case 'Backspace':
      Stop();
      break;
    case '!': // 2
      canvas.cursor_fill_skip = 1;
      canvas.draw();
      break;
    case '"': // 2
      canvas.cursor_fill_skip = 2;
      canvas.draw();
      break;
    case '£': // 3
      canvas.cursor_fill_skip = 3;
      canvas.draw();
      break;
    case '$': // 4
      canvas.cursor_fill_skip = 4;
      canvas.draw();
      break;
    case 'h': canvas.CursorMove('left'); break;
    case 'j': canvas.CursorMove('down'); break;
    case 'k': canvas.CursorMove('up'); break;
    case 'l': canvas.CursorMove('right'); break;
    case 'x':
      canvas.draw();
      break;
    case 'Shift':
      console.log('Shift pressed');
      keyShiftPressed = true;
      canvas.cursor_fill = true;
      canvas.draw();
      break;
    case 'X':
    case 'C':
      break;
    default:
      console.log('Unknown key', event.key);
  }
}, false);

document.addEventListener('keyup', (event) => {
  switch(event.key) {
    case '"': // 2
    case '£': // 3
    case '$': // 4
      break;
    case 'x':
      AddRemoveNote(canvas.GetCursor());
      canvas.draw();
      break;
    case 'X':
      AddRemoveFill(canvas.GetCursor(), true);
      canvas.draw();
      break;
    case 'C':
      AddRemoveFill(canvas.GetCursor(), false);
      canvas.draw();
      break;
    case 'Shift':
      console.log('Shift depressed');
      keyShiftPressed = false;
      canvas.cursor_fill = false;
      canvas.draw();
      break;
    default:
      console.log('Unknown key', event.key);
  }
}, false);

window.addEventListener('resize', () => {
// TODO
//  var c = document.getElementById("container");
//  c.width = window.innerWidth;
//  c.height = window.innerHeight;
//  canvas.draw();
});

function AddRemoveFill(pos, create) {
  // Add or remove a fill (does not create note if already exists)
  // TODO: create : true(create), false(remove)
  // pos: { instrument, time }
  const removeNote = (I, n) => {
    return I.filter(i => {
      return (i.time != n.time) || (i.instrument != n.instrument);
    });
  };
  if (debug) console.log(sequence);
  // Remove notes
  var sequence = seq.GetSequence();
  for (var x = 0; x < canvas.measure_len; x += canvas.cursor_fill_skip) {
    sequence = removeNote(sequence, {instrument: pos.instrument, time: x});
  }
  if (create) {
    //// Add notes
    for (var x = 0; x < canvas.measure_len; x += canvas.cursor_fill_skip) {
      sequence.push({ instrument: pos.instrument, time: x });
    }
  }
  seq.SetSequence(sequence);
}

function AddRemoveNote(pos) {
  // Add or remove a note, depending whether it exists or not
  // TODO: This should be shorter
  // pos: { instrument, time }
  const findNote = (I, x) => {
    return I.find(i => {
      return i.time == x.time && i.instrument == x.instrument;
    });
  };
  const removeNote = (I, n) => {
    return I.filter(i => {
      return (i.time != n.time) || (i.instrument != n.instrument);
    });
  };
  var sequence = seq.GetSequence();
  if (findNote(sequence, pos)) {
    // Remove note
    if (debug) console.log('Found, removing', pos);
    var newsequence = removeNote(sequence, pos);
    if (debug) console.log('REMOVED', newsequence);
    seq.SetSequence(removeNote(sequence, pos));
    if (debug) console.log(sequence);
  } else {
    // Add note
    if (debug) console.log('Not found, adding', pos);
    sequence.push(pos);
    seq.SetSequence(sequence);
  }
}

function GetSeqInstance() {
  return seq
}

// Call these functions from console for a demo
// Also, these are accessible from HTML via onclick=""
// TODO: Could be used by the front-end
function StartPause() {
  //var sequence = document.getElementById('pattern');
  //seq.SetSequenceStr(sequence.value);
  seq.StartPause();
}
function Stop() {
  seq.Stop();
}

function Help() {
  var message = `
Move cursor left/down,up,right: h,j,k,l
Start/Pause: space bar
Stop: backspace
Add/remove sound at cursor: x
Add/remove fill: Shift + x/c
Change fill: Shift + 1/2/3/4
`
  alert(message);
}

function Debug() {
  debug = !debug;
}
