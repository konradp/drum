All samples are from freesound.org, samples licenced with CreativeCommons licence.

01: Kick  
https://freesound.org/people/TheFlakesMaster/sounds/399896/

02: Snare  
https://freesound.org/people/KEVOY/sounds/82247/

03: Clap  
https://freesound.org/people/adammusic18/sounds/208875/

04: Hi-hat  
https://freesound.org/people/TheFlakesMaster/sounds/399897/
