class Canvas {
constructor() {
  // Private
  this.cursor_x = 0;
  this.cursor_y = 0;
  this.ctx = null;
  this.ctx_margin = 20;
  this.ctx_height = 0;
  this.ctx_width = 0;
  this.cursor_fill = false;
  this.cursor_fill_skip = 1;
  this.debug = false;
  this.measure_len = 16; // measure length (no. of vertical lines)
  this.sample_count = 4; // samples count (number of instruments)
  this.sequence = [];
  this.sequencer = null;

  // Constructor
  var c = document.getElementById("container");
  var ctx = c.getContext("2d");
  this.ctx = ctx;
  this.draw();
}

ConnectSequencer(sequencer) {
  this.sequencer = sequencer;
  this.draw();
}

CursorMove(dir) {
  // dir: left, right, up, down
  if (this.debug) console.log('Moving cursor', dir);
  switch(dir) {
    case 'left':
      if (this.cursor_x > 0)
        this.cursor_x--;
      else
        this.cursor_x = this.measure_len - 1;
      this.draw();
      break;
    case 'right':
      if (this.cursor_x + 1 < this.measure_len)
        this.cursor_x++;
      else
        this.cursor_x = 0;
      this.draw();
      break;
    case 'up':
      if (this.cursor_y > 0)
        this.cursor_y--;
      else
        this.cursor_y = this.sample_count - 1;
      this.draw();
      break;
    case 'down':
      if (this.cursor_y + 1 < this.sample_count)
        this.cursor_y++;
      else
        this.cursor_y = 0;
      this.draw();
      break;
    default:
      console.log('Unknown direction', dir);
  }
}

CursorFill(instrument, skip) {
  // A single cursor, no fill
  var c = document.getElementById("container");
  var ctx = c.getContext("2d");
  for (var i = 0; i < this.measure_len; i += skip) {
    var x = this.pos_to_canvas('x', i); // which beat
    var y = this.pos_to_canvas('y', this.cursor_y); // instrument number
    var size = 10;
    ctx.beginPath();
    ctx.arc(x, y, size, 0, 2*Math.PI);
    ctx.stroke();
  }
}

CursorTo(x, y) {
  // Input: x = measure, y = instrument
  this.cursor_x = x;
  this.cursor_y = y;
}

Debug() {
  this.debug = !debug;
}

GetCursor() {
  return {
    instrument: this.cursor_y,
    time: this.cursor_x
  };
}

//////////////// PRIVATE ////////////////////
// Drawing methods
draw() {
  this.clear();
  // Bounding box
  var ctx = this.ctx;
  ctx.strokeRect(0, 0, ctx.canvas.width, ctx.canvas.height);

  // From sampler
  var n = this.sample_count;
  var l = this.measure_len;

  // Draw horizontal instrument lines
  var H = ctx.canvas.height;
  var W = ctx.canvas.width;
  this.ctx_height = ctx.canvas.height;
  this.ctx_width = ctx.canvas.width;
  var m = this.ctx_margin; // margin (padding)
  for (var i = 0; i < n; i++) {
    var y = m + i*(H-2*m)/(n-1); // y position
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.moveTo(0+m, y);
    ctx.lineTo(W-m, y);
    ctx.stroke();
  }
  // Draw vertical measure lines
  ctx.lineWidth = 2;
  for (var i = 0; i < l; i++) {
    var x = m + i*(W-2*m)/(l-1); // x position
    ctx.beginPath();
    ctx.setLineDash([1, 10]);
    ctx.moveTo(x, m);
    ctx.lineTo(x, H-m);
    ctx.stroke();
    ctx.setLineDash([]);
  }

  // Draw cursor
  if(!this.cursor_fill) {
    // A single cursor, no fill
    var c = document.getElementById("container");
    var ctx = c.getContext("2d");
    var x = this.pos_to_canvas('x', this.cursor_x); // which beat
    var y = this.pos_to_canvas('y', this.cursor_y); // instrument number
    var size = 10;
    ctx.beginPath();
    ctx.arc(x, y, size, 0, 2*Math.PI);
    ctx.stroke();
  } else {
    this.CursorFill(this.cursor_y, this.cursor_fill_skip);
  }

  // Draw sequences
  this.draw_sequence();
}

draw_sequence() {
  if (this.sequencer != null) {
    var c = document.getElementById("container");
    var ctx = c.getContext("2d");
    this.sequencer.GetSequence().forEach((note) => {
      var x = this.pos_to_canvas('x', note.time); // which beat
      var y = this.pos_to_canvas('y', note.instrument); // instrument number
      var size = 6;
      ctx.beginPath();
      ctx.arc(x, y, size, 0, 2*Math.PI);
      ctx.fill();
    });
  }
}

clear() {
  var ctx = this.ctx;
  var w = this.ctx_width;
  var h = this.ctx_height;
  ctx.clearRect(0, 0, w, h); // Clear
  ctx.strokeRect(0, 0, w, h); // Bounding box
}

// Helper position methods
pos_to_canvas(type, pos) {
  // Given a beat number or instrument number, convert to position on canvas
  // Input: type = 'x' or 'y'
  var MARGIN = this.ctx_margin;
  if (type == 'x')
    var SIZE = this.ctx_width,
        LEN = this.measure_len;
  else if (type == 'y')
    var SIZE = this.ctx_height,
        LEN = this.sample_count;
  else throw 'Invalid dimension';
  if (this.debug) {
    console.log('POS:', pos,
      'POSITION:', MARGIN + pos*(SIZE - 2*MARGIN)/(LEN - 1),
      'LEN:', LEN,
      'SIZE:', SIZE,
      'MARGIN:', MARGIN);
  }
  return MARGIN + pos*(SIZE - 2*MARGIN)/(LEN - 1);
}

}; //class Canvas
