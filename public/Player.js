'use strict';

class Player {
// Load and play sounds
// Usage:
//   var p = new Player();
//   p.load_sounds([
//     ['s1', 'sounds/sound1.ogg' ],
//     ['s2', 'sounds/sound2.ogg' ]
//   ]);
//   p.play('s1');
constructor() {
  try {
    // Init AudioContext
    this.context = new AudioContext();
    if(!this.context)
      this.context = new webkitAudioContext();
    if(!this.context)
      this.context = new window.webkitAudioContext();
  } catch(e) {
    throw 'Web Audio API is not supported in this browser';
  }
  this.sounds = new Array()
}

// PUBLIC
load_sounds(sounds) {
  // Input: Array of arrays [name, url], e.g.
  // [
  //   [ 'sounds/sound1.ogg' ],
  //   [ 'sounds/sound2.ogg' ],
  //   ...
  // ]
  for (var i = 0; i < sounds.length; i++) {
    this.load_sound(i, sounds[i]);
  }
}

Play(note, time) {
  // Play note at a given time
  var audioBuffer = this.sounds[note];
  if(!audioBuffer) {
    console.log('Could not play', note);
    return false;
  }
  if (this.debug) console.log('Playing', note);
  var source = this.context.createBufferSource();
  source.buffer = audioBuffer;
  source.connect(this.context.destination); // speakers
  source.start(time);
}

//////////////// PRIVATE ////////////////////
get_url(url, callback) {
  var req = new XMLHttpRequest();
  req.open('GET', url, true);
  req.responseType = 'arraybuffer';
  req.onload = function(e) {
    var res = req.response;
    if(!res) {
      console.log('Getting url', url, 'failed:', req.statusText);
      return false;
    }
    callback(res)
    return
  }
  req.send();
}

load_sound(name, url) {
  // Load sound
  // Example: load('H', 'sounds/sound1.ogg');
  var self = this
  this.get_url(url, (data) => {
    this.context.decodeAudioData(
      data,
      (audio) => {
        self.sounds[name] = audio
      },
      (err) => { console.log('Loading failed: ', err); }
    )
  })
}

}; // class Player
